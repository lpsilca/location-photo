package utils;

public class Surface {
	private double longueur;
	private double largeur;
	/**
	 * @param longueur Longueur de la surface
	 * @param largeur	Largeur de la surface
	 */
	public Surface(double longueur, double largeur) {
		super();
		this.longueur = longueur;
		this.largeur = largeur;
	}
	/**
	 * @return the longueur
	 */
	public double getLongueur() {
		return longueur;
	}
	/**
	 * @param longueur the longueur to set
	 */
	public void setLongueur(double longueur) {
		this.longueur = longueur;
	}
	/**
	 * @return the largeur
	 */
	public double getLargeur() {
		return largeur;
	}
	/**
	 * @param largeur the largeur to set
	 */
	public void setLargeur(double largeur) {
		this.largeur = largeur;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return longueur + "x" + largeur + "";
	}
}
