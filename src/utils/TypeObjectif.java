package utils;

public enum TypeObjectif {

	OBJECTIF_A("24-32"), OBJECTIF_B("18-55"), OBJECTIF_C("12-40"), OBJECTIF_D("70-300");

	private final String objectif;

	private TypeObjectif(final String objectif) {
		this.objectif = objectif;
	}
	
	public static TypeObjectif fromString(String text) {
	    if (text != null) {
	      for (TypeObjectif t : TypeObjectif.values()) {
	        if (text.equalsIgnoreCase(t.objectif)) {
	          return t;
	        }
	      }
	    }
	    return null;
	  }
	
	public String toString(){
	    return this.objectif;
	}

}