package utils;

public class Client implements Comparable<Client> {
	private String nom;
	private String prenom;
	private String adresse;
	
	/**
	 * @param nom		Nom du client
	 * @param prenom	Prénom du client
	 * @param adresse	Adresse du client
	 */
	public Client(String nom, String prenom, String adresse) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.adresse = adresse;
	}
	
	public Client(){
		super();
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}

	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	 * @return the adresse
	 */
	public String getAdresse() {
		return adresse;
	}

	/**
	 * @param adresse the adresse to set
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Client -> " + nom + " " + prenom + " , " + adresse;
	}
	
	public String simpleToString() {
		return nom + " " + prenom;
	}

	@Override
	public int compareTo(Client c) {
		if(this.nom.equals(c.getNom()) && this.prenom.equals(c.getPrenom()) && this.adresse.equals(c.getAdresse()))
			return 1;
		else
			return -1;
	}
	
}
