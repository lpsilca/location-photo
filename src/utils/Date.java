package utils;

import java.text.ParseException;

/**
 * Copy from :
 * 	https://helloacm.com/tutorial-how-to-write-a-java-date-class-basic-oop-concept/
 * 04/10/2016 
 * 
 */

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class Date implements Comparable<Date> {
    /**
     * private attribute for holding day
     */
    private int day;

    /**
     * private attribute for holding month
     */
    private int month;

    /**
     * private attribute for holding year
     */
    private int year;

    /**
     * Constructor that takes year/month/day
     * 
     * @param year
     * @param month
     * @param day
     * @throws IllegalArgumentException
     *             if invalid
     */
    public Date(int day, int month, int year) throws IllegalArgumentException {
	if (!isValid(year, month, day))
	    throw new IllegalArgumentException();
	this.year = year;
	this.month = month;
	this.day = day;
    }

    /**
     * default constructor that uses today's Date()
     */
    public Date() {
	Calendar currentDate = Calendar.getInstance(); // Get the current date
	java.util.Date x = currentDate.getTime();
	SimpleDateFormat formatyear = new SimpleDateFormat("yyyy");
	this.year = Integer.parseInt(formatyear.format(x));
	SimpleDateFormat formatmonth = new SimpleDateFormat("MM");
	this.month = Integer.parseInt(formatmonth.format(x));
	SimpleDateFormat formatdd = new SimpleDateFormat("dd");
	this.day = Integer.parseInt(formatdd.format(x));
    }

    /**
     * Getter for day
     * 
     * @return day
     */
    public int getDay() {
	return this.day;
    }

    /**
     * Getter for month
     * 
     * @return month
     */
    public int getMonth() {
	return this.month;
    }

    /**
     * Getter pour le mois version texte
     * 
     * @return String
     */
    public String getMonthText() {
	switch (this.month) {
	case 1:
	    return "Janvier";
	case 2:
	    return "Février";
	case 3:
	    return "Mars";
	case 4:
	    return "Avril";
	case 5:
	    return "Mai";
	case 6:
	    return "Juin";
	case 7:
	    return "Juillet";
	case 8:
	    return "Août";
	case 9:
	    return "Septembre";
	case 10:
	    return "Octobre";
	case 11:
	    return "Novembre";
	default:
	    return "Décembre";
	}
    }

    /**
     * Getter for year
     * 
     * @return year
     */
    public int getYear() {
	return this.year;
    }

    /**
     * Setter for day
     */
    public void setDay(int day) {
	this.day = day;
    }

    /**
     * Setter for month
     */
    public void setMonth(int month) {
	this.month = month;
    }

    /**
     * Setter for year
     */
    public void setYear(int year) {
	this.year = year;
    }

    /**
     * Compare two dates
     * 
     * @param d
     *            Date
     * @return true if it is earlier than d
     */
    public int compareTo(Date d) {
	int day1 = d.getDay();
	int month1 = d.getMonth();
	int year1 = d.getYear();
	if ((this.year == year1) && (this.month == month1) && (this.day == day1))
	    return 0;
	else if (this.year > year1)
	    return 1;
	else if (this.year == year1 && this.month > month1)
	    return 1;
	else if (this.year == year1 && this.month == month1 && this.day > day1)
	    return 1;
	else
	    return -1;

    }

    /**
     * Check if given year/month/day is valid
     * 
     * @param year
     * @param month
     * @param day
     * @return true if it is valid date
     */
    public static boolean isValid(int year, int month, int day) {
	if (year < 0)
	    return false;
	if ((month < 1) || (month > 12))
	    return false;
	if ((day < 1) || (day > 31))
	    return false;
	switch (month) {
	case 1:
	    return true;
	case 2:
	    return (isLeap(year) ? day <= 29 : day <= 28);
	case 3:
	    return true;
	case 4:
	    return day < 31;
	case 5:
	    return true;
	case 6:
	    return day < 31;
	case 7:
	    return true;
	case 8:
	    return true;
	case 9:
	    return day < 31;
	case 10:
	    return true;
	case 11:
	    return day < 31;
	default:
	    return true;
	}
    }

    /**
     * Check given year is leap year
     * 
     * @param year
     * @return true if year is leap year
     */
    public static boolean isLeap(int year) {
	// using system library to do this, avoid re-invent the wheel
	Calendar cal = Calendar.getInstance();
	cal.set(Calendar.YEAR, year);
	return cal.getActualMaximum(Calendar.DAY_OF_YEAR) > 365;
    }

    /**
     * Give number of day between two date copy from
     * http://stackoverflow.com/questions/20165564/calculating-days-between-two-dates-with-in-java
     * 04/10/2016
     * 
     * @param dateEnd
     * @return nbDays | -1 if error parsing
     */
    public long diffJour(Date dateEnd) {
	SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");
	try {
	    java.util.Date date1 = myFormat.parse(this.toString());
	    java.util.Date date2 = myFormat.parse(dateEnd.toString());
	    long diff = date2.getTime() - date1.getTime();
	    return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	} catch (ParseException e) {
	    e.printStackTrace();
	    return -1;
	}
    }

    /**
     * Indique si deux dates sont égaux
     * 
     * @param date
     *            Date
     * @return boolean
     */
    public boolean equals(Date date) {
	return this == date
		|| (this.day == date.getDay() && this.month == date.getMonth() && this.year == date.getYear());
    }

    /**
     * return a string representation convert to ISO 8601 Date Format
     * http://en.wikipedia.org/wiki/ISO_8601
     * 
     * @return
     */
    public String toString() {
	StringBuilder s = new StringBuilder();
	if (this.day < 10)
	    s.append("0");
	s.append(String.valueOf(this.day));
	s.append("-");
	if (this.month < 10)
	    s.append("0");
	s.append(String.valueOf(this.month));
	s.append("-");
	s.append(String.valueOf(this.year));
	return s.toString();
    }
}
