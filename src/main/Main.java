package main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Scanner;

import Exception.ArticleNotFoundException;
import Exception.ArticleOutOfStockException;
import Exception.ClientNotFoundException;
import Exception.DateErrorException;
import Exception.DateWrongFormatException;
import Exception.LocationNotFoundException;
import Magasin.Magasin;
import utils.Client;
import utils.Date;

public class Main {
    // Utilisé pour les saisies de l'utilisateur
    public static Scanner sc = new Scanner(System.in);

    private static Magasin m;

    private static Date dateOfToday;

    public static void main(String[] args) {

	m = new Magasin();

	Boolean exit = false;
	System.out.println("Bienvenue sur l'application de gestion de location");
	try {
	    System.out.println("Configuration de l'application...");
	    createBrandCode(m);
	    createArticle(m);
	    fullData();
	    // Instancie la date du jour
	    String[] dateStr;
	    // Récupération de la date du jour
	    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    Calendar cal = Calendar.getInstance();
	    dateStr = dateFormat.format(cal.getTime()).split("/");
	    dateOfToday = new Date(Integer.valueOf(dateStr[0]), Integer.valueOf(dateStr[1]),
		    Integer.valueOf(dateStr[2]));
	    System.out.println("Configuration réussie");
	    do {
		// Menu Principal
		switch (menu(0)) {
		case 0:
		    exit = true;
		    break;
		case 1:
		    menuClient();
		    break;
		case 2:
		    menuLocation();
		    break;
		case 3:
		    m.afficherArticles();
		    break;
		case 4:
		    emptyLineConsole(2);
		    System.out.println("### Affichage du CA ###");
		    emptyLineConsole(2);
		    try {
			System.out.println("Date début (MM/AAAA) :");
			dateStr = sc.next().split("/");
			// Test le bon format de la date soit JJ/MM/AAAA
			if (dateStr.length != 2 || dateStr[0].length() != 2 || dateStr[1].length() != 4) {
			    throw new DateWrongFormatException();
			}
			Date dateDebut = new Date(1, Integer.valueOf(dateStr[0]), Integer.valueOf(dateStr[1]));
			System.out.println("Date Fin (MM/AAAA) :");
			dateStr = sc.next().split("/");
			// Test le bon format de la date soit JJ/MM/AAAA
			if (dateStr.length != 2 || dateStr[0].length() != 2 || dateStr[1].length() != 4) {
			    throw new DateWrongFormatException();
			}
			Date dateFin = new Date(1, Integer.valueOf(dateStr[0]), Integer.valueOf(dateStr[1]));
			if (dateDebut.diffJour(dateOfToday) < 0 || dateFin.diffJour(dateOfToday) < 0) {
			    throw new DateErrorException("Les dates données sont supérieur à celle d'ajourd'hui");
			}
			System.out.println("Le chiffre d'affaire du " + dateDebut + " au " + dateFin + " est de "
				+ m.getCA(dateDebut, dateFin) + " €");
		    } catch (DateWrongFormatException e) {
			System.err.println(e.getMessage());
		    } catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		    } catch (DateErrorException e) {
			System.err.println(e.getMessage());
		    }
		    break;
		default:
		    System.err.println("Commande inconnue !!!");
		    break;
		}
	    } while (!exit);
	} catch (IOException e) {
	    System.err.println("Erreur lors de la configuration de l'application. \n"
		    + " Vérifer que les fichiers \"Articles.txt\" et \"Brand-Code.txt\" \n"
		    + " sont bien présent dans le dossier \"conf\"");
	} finally {
	    sc.close();
	}
    }

    /**
     * Remplie l'application des marques et leur code renseigner dans le fichier
     * "conf/Brand-Code.conf"
     * 
     * @param m
     *            Magasin
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void createBrandCode(Magasin m) throws FileNotFoundException, IOException {
	try (BufferedReader br = new BufferedReader(new FileReader("conf/Brand-Code.conf"))) {
	    String line;
	    while ((line = br.readLine()) != null) {
		if (!line.contains("//"))
		    m.ajouterMarqueCode(line.split("-")[0], line.split("-")[1]);
	    }
	}
    }

    /**
     * Remplie l'application des articles renseigner dans le fichier
     * "conf/Articles.conf"
     * 
     * @param m
     *            Magasin
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void createArticle(Magasin m) throws FileNotFoundException, IOException {
	try (BufferedReader br = new BufferedReader(new FileReader("conf/Articles.conf"))) {
	    String line;
	    while ((line = br.readLine()) != null) {
		if (!line.contains("//")) {
		    LinkedList<String> attributs = new LinkedList<String>(Arrays.asList(line.split("-")));
		    m.ajouterArticleStock(attributs.pop(), attributs.pop(), attributs.pop(),
			    Integer.valueOf(attributs.pop()), Integer.valueOf(attributs.pop()), attributs.toArray());
		}
	    }
	}
    }

    /**
     * Jeu d'essaie, remplie le magasin de client et de location
     */
    public static void fullData() {
	m.ajouterClient(new Client("DUPONT", "Jean", "3 rue du pont-levis"));
	m.ajouterClient(new Client("MARTIN", "René", "26 rue des acacias"));
	m.ajouterClient(new Client("DURAND", "Pierre", "41 impasse des crapauds"));
	m.ajouterClient(new Client("MOREAU", "Paul", "1 bis Avenue Charle de Gaule"));
	m.ajouterClient(new Client("MOREL", "Philipe", "114 Boulevard des poilus"));

	LinkedList<String> articleList = new LinkedList<String>();

	try {
	    articleList.add("MT-0");
	    articleList.add("TT-0");
	    articleList.add("NK-1");
	    m.enregistrerLocation(m.getClient(0), articleList, new Date(3, 9, 2016), new Date(15, 9, 2016));
	    articleList = new LinkedList<String>();

	    articleList.add("PF-2");
	    articleList.add("BM-0");
	    articleList.add("RD-2");
	    m.enregistrerLocation(m.getClient(2), articleList, new Date(12, 1, 2017), new Date(15, 1, 2017));
	    articleList = new LinkedList<String>();

	    articleList.add("PT-0");
	    articleList.add("NK-2");
	    m.enregistrerLocation(m.getClient(2), articleList, new Date(30, 1, 2017), new Date(5, 2, 2017));
	} catch (IllegalArgumentException e) {
	    System.err.println("La date est incorrecte");
	} catch (ClientNotFoundException e) {
	    System.err.println(e.getMessage());
	} catch (IOException e) {
	    System.err.println(e.getMessage());
	} catch (ArticleNotFoundException e) {
	    System.err.println(e.getMessage());
	} catch (ArticleOutOfStockException e) {
	    System.err.println(e.getMessage());
	}
    }

    /**
     * Affiche les menus
     * 
     * @param num
     *            int
     * @return int
     */
    public static int menu(int num) {
	emptyLineConsole(4);
	int code = -1;
	switch (num) {
	case 0:
	    System.out.println("### Menu Principal ###");
	    System.out.println("Que souhaitez-vous faire (numéro) ?");
	    emptyLineConsole(2);
	    System.out.println("1- Menu Client");
	    System.out.println("2- Menu Location");
	    System.out.println("3- Afficher la liste des articles");
	    System.out.println("4- Consulter le chiffre d'affaire d'une période");
	    System.out.println("0- Quitter");
	    code = sc.nextInt();
	    break;
	case 1:
	    System.out.println("### Menu Client ###");
	    System.out.println("Que souhaitez-vous faire (numéro) ?");
	    emptyLineConsole(2);
	    System.out.println("1- Afficher la liste des clients");
	    System.out.println("2- Afficher les locations d'un client");
	    System.out.println("3- Ajouter un client");
	    System.out.println("4- Supprimer un client");
	    System.out.println("0- Retour");
	    code = sc.nextInt();
	    break;
	case 2:
	    System.out.println("### Menu Location ###");
	    System.out.println("Que souhaitez-vous faire (numéro) ?");
	    emptyLineConsole(2);
	    System.out.println("1- Enregistrer une location");
	    System.out.println("2- Afficher la liste des locations en cours");
	    System.out.println("3- Afficher une archive");
	    System.out.println("4- Terminer une location");
	    System.out.println("0- Retour");
	    code = sc.nextInt();
	    break;
	}
	return code;
    }

    /**
     * Gestion des actions du menu client
     */
    public static void menuClient() {
	Boolean back = false;
	do {
	    // Menu Client
	    switch (menu(1)) {
	    // Retour
	    case 0:
		back = true;
		break;
	    // Afficher la liste des clients
	    case 1:
		m.afficherListeClient();
		break;
	    // Afficher les locations d'un client
	    case 2:
		System.out.println("Affichage des locations d'un client");
		System.out.print("ID du client : ");
		try {
		    m.afficherLocationClient(m.getClient(sc.nextInt()));
		} catch (ClientNotFoundException e) {
		    System.err.println(e.getMessage());
		}
		break;
	    // Ajouter un client
	    case 3:
		Client c = new Client();
		System.out.println("Création d'un nouveau client");
		sc.nextLine();
		System.out.print("Nom : ");
		c.setNom(sc.nextLine());
		System.out.print("Prénom : ");
		c.setPrenom(sc.nextLine());
		System.out.print("Adresse : ");
		c.setAdresse(sc.nextLine());
		System.out.println(c);
		m.ajouterClient(c);
		break;
	    // Supprimer un client
	    case 4:
		System.out.print("Saisisser l'id du client : ");
		try {
		    m.supprimerClient(sc.nextInt());
		} catch (ClientNotFoundException e) {
		    System.err.println(e.getMessage());
		}
		break;
	    }
	} while (!back);
    }

    /**
     * Gestion des action du menu location
     * 
     * @throws IOException
     */
    public static void menuLocation() throws IOException {
	Boolean back = false;
	do {
	    // Menu Location
	    switch (menu(2)) {
	    case 0:
		back = true;
		break;
	    // Enregistrement d'une location
	    case 1:
		emptyLineConsole(3);
		System.out.println("### Enregistrement d'une location ###");
		emptyLineConsole(1);
		try {
		    String[] dateStr;
		    System.out.print("Date de début (" + dateOfToday.getDay() + "/" + dateOfToday.getMonth() + "/"
			    + dateOfToday.getYear() + ")\n");
		    System.out.print("Date de fin (JJ/MM/AAAA) : ");
		    dateStr = sc.next().split("/");
		    // Test le bon format de la date soit JJ/MM/AAAA
		    if (dateStr.length != 3 || dateStr[0].length() != 2 || dateStr[1].length() != 2
			    || dateStr[2].length() != 4) {
			throw new DateWrongFormatException();
		    }
		    Date dateFin = new Date(Integer.valueOf(dateStr[0]), Integer.valueOf(dateStr[1]),
			    Integer.valueOf(dateStr[2]));
		    String saisie = "";
		    LinkedList<String> listArticle = new LinkedList<String>();
		    m.afficherArticles();
		    while (saisie != "q") {
			System.out.println(
				"Quel article ajouter (Ref) {q : fin | m : afficher articles | l : ma liste} ?");
			saisie = sc.next();
			if (m.articleExist(saisie)) {
			    listArticle.add(saisie);
			} else if (saisie.equals("m")) {
			    m.afficherArticles();
			} else if (!saisie.equals("q")) {
			    System.out.println("Article inconnue");
			}
		    }
		    m.afficherListeClient();
		    System.out.println("Quel est le client (id) ?");
		    m.enregistrerLocation(m.getClient(sc.nextInt()), listArticle, dateOfToday, dateFin);
		} catch (IllegalArgumentException e) {
		    System.err.println("La date est incorrecte");
		} catch (ClientNotFoundException e) {
		    System.err.println(e.getMessage());
		} catch (DateWrongFormatException e) {
		    System.err.println(e.getMessage());
		} catch (ArticleNotFoundException e) {
		    System.err.println(e.getMessage());
		} catch (ArticleOutOfStockException e) {
		    System.err.println(e.getMessage());
		}
		break;
	    // Afficher la liste des locations en cours
	    case 2:
		emptyLineConsole(3);
		System.out.println("### Affichage de la liste des locations en cours ###");
		emptyLineConsole(1);
		m.afficherListeLocation();
		break;
	    // Afficher une archive
	    case 3:
		emptyLineConsole(3);
		System.out.println("### Affichage d'une archive ###");
		emptyLineConsole(1);
		System.out.println("Année et mois de l'archive (MM/AAAA) ?");
		try {
		    String[] saisie = sc.next().split("/");
		    if (saisie.length != 2 || saisie[0].length() != 2 || saisie[1].length() != 4) {
			throw new DateWrongFormatException();
		    }
		    Date dateTest = new Date(1, Integer.valueOf(saisie[0]), Integer.valueOf(saisie[1]));
		    if (dateTest.diffJour(dateOfToday) < 0) {
			throw new DateErrorException("La date donnée est supérieur à celle d'aujourd'hui");
		    }
		    m.afficherArchive(Integer.valueOf(saisie[1]), Integer.valueOf(saisie[0]));
		} catch (DateWrongFormatException e) {
		    System.err.println(e.getMessage());
		} catch (DateErrorException e) {
		    System.err.println(e.getMessage());
		}
		break;
	    // Termine une location
	    case 4:
		emptyLineConsole(3);
		System.out.println("### Fin d'une location ###");
		emptyLineConsole(1);
		m.afficherListeLocation();
		System.out.println("Numéro de la location à archiver ?");
		try {
		    m.terminerLocation(m.getLocation(sc.nextInt()));
		} catch (LocationNotFoundException e) {
		    System.err.println(e.getMessage());
		} catch (ArticleNotFoundException e) {
		    System.err.println(e.getMessage());
		} catch (IOException e) {
		    System.err.println(e.getMessage());
		}
		break;
	    default:
		System.err.println("Commande inconnue !!!");
		break;
	    }
	} while (!back);
    }

    /**
     * Affiche des retour à la ligne
     * 
     * @param nb
     *            int nombre de retour à la ligne voulue
     */
    public static void emptyLineConsole(int nb) {
	String str = "";
	for (int i = 0; i < nb; i++) {
	    str += "\n";
	}
	System.out.println(str);
    }
}
