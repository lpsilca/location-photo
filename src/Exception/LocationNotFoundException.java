package Exception;

public class LocationNotFoundException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -3406907721231289394L;

    public LocationNotFoundException() {
	super("La location n'existe pas !");
    }

}
