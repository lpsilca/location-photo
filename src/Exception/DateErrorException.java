package Exception;

public class DateErrorException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -8241685753573526804L;

    public DateErrorException(String message) {
	super(message);
    }

}
