package Exception;

public class DateWrongFormatException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 6177823408898858807L;

    public DateWrongFormatException() {
	super("La date doit être au format JJ/MM/AAAA");
    }

}
