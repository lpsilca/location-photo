package Exception;

public class ArticleNotFoundException extends Exception {

    /**
    * 
    */
    private static final long serialVersionUID = -9086854288388149707L;

    public ArticleNotFoundException(String refArticle) {
	super("L'article "+refArticle+" n'existe pas !");
    }

}
