package Exception;

public class ClientNotFoundException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -7965839346124933538L;

    public ClientNotFoundException() {
	super("Le client n'existe pas !");
    }

}
