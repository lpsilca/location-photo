package Exception;

public class ArticleOutOfStockException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -4352887048080829654L;

    public ArticleOutOfStockException(String refArticle) {
	super("L'article "+refArticle+" n'est plus disponible en stock !");
    }

}
