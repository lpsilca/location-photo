package Magasin;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import Article.AccessoireSon;
import Article.Article;
import Article.DispositifAcquisition;
import Article.FondStudio;
import Article.MaterielTournage;
import Article.Objectif;
import Article.PanneauLed;
import Article.Reflecteur;
import Exception.ArticleNotFoundException;
import Exception.ArticleOutOfStockException;
import Exception.ClientNotFoundException;
import Exception.LocationNotFoundException;
import utils.Client;
import utils.Date;
import utils.Surface;
import utils.TypeObjectif;

public class Magasin {
    // Stockage de la correspondance du code la marque et de son nom
    private HashMap<String, String> refMarqueCode;
    // Liste des articles en stock
    private HashMap<String, HashMap<Integer, Article>> stock;
    // Liste des locations non terminés
    private LinkedList<Location> listLocationEnCours;
    // Liste des clients du magasin
    private LinkedList<Client> listClients;

    public Magasin() {
	super();
	this.refMarqueCode = new HashMap<String, String>();
	this.stock = new HashMap<String, HashMap<Integer, Article>>();
	this.listLocationEnCours = new LinkedList<Location>();
	this.setListClients(new LinkedList<Client>());
    }

    /**
     * Ajoute une marque et son code
     * 
     * @param nomMarque
     *            String
     * @param code
     *            String code correspondant à la marque
     */
    public void ajouterMarqueCode(String nomMarque, String code) {
	this.refMarqueCode.put(code, nomMarque);
	this.stock.put(code, new HashMap<Integer, Article>());
    }

    /**
     * Récupère le code d'une marque
     * 
     * @param marque
     *            String
     * @return String
     */
    public String getCodeOfMarque(String marque) {
	for (String codeMagasin : this.refMarqueCode.keySet()) {
	    if (marque.equals(this.refMarqueCode.get(codeMagasin)))
		return codeMagasin;
	}
	return null;
    }

    /**
     * Récupère la marque selon le code
     * 
     * @param code
     *            String
     * @return String
     */
    public String getMarqueOfCode(String code) {
	return this.refMarqueCode.get(code);
    }

    /**
     * Permet d'ajouter un article au stock du magasin
     * 
     * @param objet
     *            String Intitulé de la classe de l'objet
     * @param codeMarque
     *            String Code identifiant la marque
     * @param nom
     *            String Nom de l'article
     * @param prix
     *            String Prix de l'article
     * @param nbStock
     *            double Quantité d'article en stock
     * @param attributs
     *            Object[] Attribut qui dépend du type de l'article
     */
    public void ajouterArticleStock(String objet, String codeMarque, String nom, double prix, int nbStock,
	    Object[] attributs) {
	// Récupération du HashMap qui représente les articles de la marque
	HashMap<Integer, Article> stockMarque = this.stock.get(codeMarque);
	// On créer l'instance qui convient pour chaque articles
	switch (objet) {
	case "PanneauLed":
	    stockMarque.put(stockMarque.size(), new PanneauLed((codeMarque + "-" + stockMarque.size()),
		    getMarqueOfCode(codeMarque), nom, prix, nbStock, Integer.valueOf((String) attributs[0])));
	    break;
	case "Reflecteur":
	    stockMarque.put(stockMarque.size(), new Reflecteur((codeMarque + "-" + stockMarque.size()),
		    getMarqueOfCode(codeMarque), nom, prix, nbStock,
		    new Surface(Integer.valueOf((String) attributs[0]), Integer.valueOf((String) attributs[1]))));
	    break;
	case "FondStudio":
	    stockMarque.put(stockMarque.size(), new FondStudio((codeMarque + "-" + stockMarque.size()),
		    getMarqueOfCode(codeMarque), nom, prix, nbStock,
		    new Surface(Integer.valueOf((String) attributs[0]), Integer.valueOf((String) attributs[1]))));
	    break;
	case "MaterielTournage":
	    stockMarque.put(stockMarque.size(), new MaterielTournage((codeMarque + "-" + stockMarque.size()),
		    getMarqueOfCode(codeMarque), nom, prix, nbStock));
	    break;
	case "DispositifAcquisition":
	    stockMarque.put(stockMarque.size(),
		    new DispositifAcquisition((codeMarque + "-" + stockMarque.size()), getMarqueOfCode(codeMarque), nom,
			    prix, nbStock, Double.valueOf((String) attributs[0]),
			    TypeObjectif.valueOf((String) attributs[1])));
	    break;
	case "AccessoireSon":
	    stockMarque.put(stockMarque.size(), new AccessoireSon((codeMarque + "-" + stockMarque.size()),
		    getMarqueOfCode(codeMarque), nom, prix, nbStock));
	    break;
	case "Objectif":
	    stockMarque.put(stockMarque.size(), new Objectif((codeMarque + "-" + stockMarque.size()),
		    getMarqueOfCode(codeMarque), nom, prix, nbStock, TypeObjectif.valueOf((String) attributs[0])));
	    break;
	}
	// On réinjecte le HashMap avec l'article en plus
	this.stock.put(codeMarque, stockMarque);
    }

    /**
     * Affiche les articles disponible
     * 
     */
    public void afficherArticles() {
	for (String marque : this.stock.keySet()) {
	    for (Integer key : this.stock.get(marque).keySet()) {
		System.out.println(this.stock.get(marque).get(key));
	    }
	}
    }

    /**
     * Vérifie si l'article existe dans le magasin
     * 
     * @param refArticle
     *            String " #CodeMarque#-#idArticle# "
     * @return boolean
     */
    public boolean articleExist(String refArticle) {
	if (this.stock.containsKey(refArticle.split("-")[0])) {
	    if (this.stock.get(refArticle.split("-")[0]).containsKey(Integer.valueOf(refArticle.split("-")[1]))) {
		return true;
	    } else {
		return false;
	    }
	} else {
	    return false;
	}
    }

    public Article getArticle(String refArticle) throws ArticleNotFoundException {
	if (articleExist(refArticle)) {
	    String marque = refArticle.split("-")[0];
	    int idArticle = Integer.valueOf(refArticle.split("-")[1]);
	    return this.stock.get(marque).get(idArticle);
	} else {
	    throw new ArticleNotFoundException(refArticle);
	}
    }

    /**
     * Enregistre la location dans le magasin
     * 
     * @param client
     *            Client client réalisant la location
     * @param refArticles
     *            List<String> liste des références des articles
     * @param dateDebut
     *            Date
     * @param dateFin
     *            Date
     * @throws IOException
     * @throws ArticleNotFoundException
     * @throws ArticleOutOfStockException
     */
    public void enregistrerLocation(Client client, List<String> refArticles, Date dateDebut, Date dateFin)
	    throws IOException, ArticleNotFoundException, ArticleOutOfStockException {
	for (String refArticle : refArticles) {
	    if (!encoreEnStock(refArticle)) {
		throw new ArticleOutOfStockException(refArticle);
	    }
	}
	this.listLocationEnCours.add(new Location(dateDebut, dateFin,
		calculMontant(dateDebut.diffJour(dateFin), refArticles), client, refArticles));
	for (String refArticle : refArticles) {
	    retireStock(refArticle);
	}
    }

    /**
     * Calcul le montant de la location avec tous les articles
     * 
     * @param nbJour
     *            int
     * @param listRefArticles
     *            List<String> contient la référence de tous les articles
     * @return
     */
    public double calculMontant(long nbJour, List<String> listRefArticles) {
	double montant = 0;
	for (String ref : listRefArticles) {
	    try {
		montant += getArticle(ref).getPrix() * nbJour;
	    } catch (ArticleNotFoundException e) {
		System.err.println(e.getMessage());
	    }
	}
	return montant;
    }

    /**
     * Permet d'incrémenter le stock de 1 pour un articles
     * 
     * @param refArticle
     *            String
     * @throws ArticleNotFoundException
     */
    public void ajouteStock(String refArticle) throws ArticleNotFoundException {
	Article curArticle = getArticle(refArticle);
	curArticle.setNbStock(curArticle.getNbStock() + 1);
    }

    /**
     * Permet décrémenter le stock de 1 pour un articles
     * 
     * @param refArticle
     *            String
     * @throws ArticleNotFoundException
     */
    public void retireStock(String refArticle) throws ArticleNotFoundException {
	Article curArticle = getArticle(refArticle);
	curArticle.setNbStock(curArticle.getNbStock() - 1);
    }

    /**
     * Indique s'il y a encore des articles disponoble en stock
     * 
     * @param refArticle
     *            String
     * @throws ArticleNotFoundException
     */
    public boolean encoreEnStock(String refArticle) throws ArticleNotFoundException {
	return getArticle(refArticle).getNbStock() > 0;
    }

    /**
     * Affiche l'ensemble des locations pour un client donné
     * 
     * @param client
     *            Client
     */
    public void afficherLocationClient(Client client) {
	boolean value = false;
	for (Location l : this.listLocationEnCours) {
	    if (l.getClient().equals(client)) {
		value = true;
		System.out.println(l);
	    }
	}
	if (!value) {
	    System.out.println("Ce client n'a aucune location en cours !");
	}
    }

    /**
     * Affiche la liste des clients avec leur ID
     */
    public void afficherListeClient() {
	int i = 0;
	for (Client c : this.listClients) {
	    System.out.print(i + " | ");
	    i++;
	    System.out.println(c);
	}
    }

    /**
     * Récupère un client
     * 
     * @param id
     *            int
     * @return Client
     * @throws ClientNotFoundException
     */
    public Client getClient(int id) throws ClientNotFoundException {
	if (this.listClients.size() > id)
	    return this.listClients.get(id);
	else
	    throw new ClientNotFoundException();
    }

    /**
     * Affiche un client
     * 
     * @param id
     *            int
     */
    public void afficherClient(int id) {
	System.out.println(this.listClients.get(id));
    }

    /**
     * Ajoute un client à la liste
     * 
     * @param c
     *            Client
     */
    public void ajouterClient(Client c) {
	this.listClients.add(c);
    }

    /**
     * Supprime un client de la liste
     * 
     * @param id
     *            int
     * @throws ClientNotFoundException
     */
    public void supprimerClient(int id) throws ClientNotFoundException {
	if (this.listClients.size() > id)
	    this.listClients.remove(id);
	else
	    throw new ClientNotFoundException();
    }

    public void afficherListeLocation() {
	int i = 0;
	for (Location l : this.listLocationEnCours) {
	    System.out.print(i + " | ");
	    i++;
	    System.out.println(l);
	}
    }

    public Location getLocation(int num) throws LocationNotFoundException {
	System.out.println(this.listLocationEnCours);
	if (this.listLocationEnCours.get(num) != null) {
	    return this.listLocationEnCours.get(num);
	} else {
	    throw new LocationNotFoundException();
	}
    }

    /**
     * Termine une location
     * 
     * @param location
     * @throws ArticleNotFoundException
     * @throws IOException
     */
    public void terminerLocation(Location location) throws ArticleNotFoundException, IOException {
	String[] dateStr;
	// Récupération de la date du jour
	DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	Calendar cal = Calendar.getInstance();
	dateStr = dateFormat.format(cal.getTime()).split("/");
	Date dateToday = new Date(Integer.valueOf(dateStr[0]), Integer.valueOf(dateStr[1]),
		Integer.valueOf(dateStr[2]));
	// Affiche un message en cas de location terminer à une date différente
	// de celle prévu
	if (!location.getDateFin().equals(dateToday)) {
	    long dateDiff = location.getDateFin().diffJour(dateToday);
	    if (dateDiff < 0)
		System.out.println("Attention la location est rendu avec " + Math.abs(dateDiff) + " Jour d'avance");
	    else
		System.out.println("Attention la location est rendu avec " + dateDiff + " Jour de retard");
	}
	archiverLocation(location);
	for (String refArticle : location.getRefArticles()) {
	    ajouteStock(refArticle);
	}
	this.listLocationEnCours.remove(location);
    }

    /**
     * Ajoute dans un fichier la description de la location
     * 
     * @param location
     *            Location location à archiver
     */
    public void archiverLocation(Location location) throws IOException {
	String nomFichier = location.getDateFin().getYear() + "" + location.getDateFin().getMonth();
	File file = new File("archive/" + nomFichier + ".loc");
	if (!file.exists()) {
	    // Créer le dossié "archive" s'il n'existe pas
	    if (!new File("archive").exists())
		if (!new File("archive").mkdir())
		    throw new IOException("Impossible de créer le dossier archive");
	    if (!file.createNewFile()) {
		throw new IOException("Impossible de créer le fichier");
	    }
	}
	DataOutputStream fluxSortieBinaire = new DataOutputStream(new FileOutputStream(file, true));
	String data = location.toArchive();
	char[] charArray = data.toCharArray();
	for (int c : charArray) {
	    fluxSortieBinaire.writeChar(c);
	}
	fluxSortieBinaire.writeChar(';');
	// fluxSortieBinaire.writeChars(data);
	// System.out.println(data);

	fluxSortieBinaire.close();
    }

    /**
     * Affiche le contenu d'une archive selon une année et un mois
     * 
     * @param year
     *            int
     * @param month
     *            int
     * @throws IOException
     */
    public void afficherArchive(int year, int month) throws IOException, FileNotFoundException {
	try {
	    DataInputStream fluxEntreeBinaireArchive = new DataInputStream(
		    new BufferedInputStream(new FileInputStream("archive/" + year + "" + month + ".loc")));
	    try {
		String str = "";
		char caractere = 0;
		while (true) {
		    if (caractere == ';') {
			System.out.println(str);
			str = "";
		    }
		    caractere = fluxEntreeBinaireArchive.readChar();
		    str += caractere;
		}
	    } catch (EOFException e) {
		// Récupère la fin de fichier
	    } catch (IOException e) {
		System.out.println("erreur d'E/S");
	    }
	    fluxEntreeBinaireArchive.close();
	} catch (FileNotFoundException e) {
	    System.out.println("le fichier d'archive n'existe pas");
	}
    }

    /**
     * Retourne le CA sur une période donnée (période en mois)
     * 
     * @param dateDebut
     * @param dateFin
     * @throws IOException
     */
    public double getCA(Date dateDebut, Date dateFin) throws IOException {
	double ca = 0;
	int moisDebut = dateDebut.getMonth();
	int anneeDebut = dateDebut.getYear();

	Date dateEnCours = new Date(1, moisDebut, anneeDebut);
	while (dateEnCours.compareTo(dateFin) == -1) {
	    File f = new File("archive/" + dateEnCours.getYear() + "" + dateEnCours.getMonth() + ".loc");
	    // Test s'il y a une archive
	    if (!f.isFile()) {
		throw new FileNotFoundException("Il n'y a pas d'archive pour le mois " + dateEnCours.getMonthText()
			+ " en " + dateEnCours.getYear());
	    }
	    DataInputStream fluxEntreeBinaireArchive = new DataInputStream(
		    new BufferedInputStream(new FileInputStream(f)));
	    try {
		String str = "";
		char caractere = 0;
		double montant = 0;
		while (true) {
		    if (caractere == ';') {
			montant = Double.valueOf(str.split("_")[0]);
			ca += montant;
			str = "";
		    }
		    caractere = fluxEntreeBinaireArchive.readChar();
		    str += caractere;

		}
	    } catch (EOFException e) {
		// Récupère la fin de fichier
	    } catch (IOException e) {
		System.out.println("erreur d'E/S");
	    }
	    fluxEntreeBinaireArchive.close();

	    int yearDateEnCours = dateEnCours.getYear();
	    int monthDateEnCours = dateEnCours.getMonth();
	    if (dateEnCours.getMonth() == 12) {
		dateEnCours.setYear(yearDateEnCours + 1);
		dateEnCours.setMonth(1);
	    } else {
		dateEnCours.setMonth(monthDateEnCours + 1);
	    }

	}
	return ca;
    }

    /**
     * @return the listLocation
     */
    public LinkedList<Location> getListLocationEnCours() {
	return listLocationEnCours;
    }

    /**
     * @param listLocation
     *            the listLocation to set
     */
    public void setListLocation(LinkedList<Location> listLocationEnCours) {
	this.listLocationEnCours = listLocationEnCours;
    }

    /**
     * @return the refMarqueCode
     */
    public HashMap<String, String> getRefMarqueCode() {
	return refMarqueCode;
    }

    /**
     * @param refMarqueCode
     *            the refMarqueCode to set
     */
    public void setRefMarqueCode(HashMap<String, String> refMarqueCode) {
	this.refMarqueCode = refMarqueCode;
    }

    /**
     * @return the stock
     */
    public HashMap<String, HashMap<Integer, Article>> getStock() {
	return stock;
    }

    /**
     * @return the listClients
     */
    public LinkedList<Client> getListClients() {
	return listClients;
    }

    /**
     * @param listClients
     *            the listClients to set
     */
    public void setListClients(LinkedList<Client> listClients) {
	this.listClients = listClients;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "Magasin [listLocation=" + listLocationEnCours + "]";
    }
}
