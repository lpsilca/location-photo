package Magasin;

import java.util.List;

import utils.Client;
import utils.Date;

public class Location {

    private Date dateDebut;
    private Date dateFin;
    private double montant;
    private Client client;
    private List<String> refArticles;

    /**
     * @param dateDebut
     *            Date de début de la location
     * @param dateFin
     *            Date de fin de la location
     * @param montant
     *            Montant de la location
     * @param client
     *            Client qui loue les articles
     * @param articles
     *            Ensemble des articles de la location
     */
    public Location(Date dateDebut, Date dateFin, double montant, Client client, List<String> refArticles) {
	super();
	this.dateDebut = dateDebut;
	this.dateFin = dateFin;
	this.montant = montant;
	this.client = client;
	this.refArticles = refArticles;
    }

    /**
     * Constructeur sans paramètre
     */
    public Location() {
	super();
    }

    /**
     * @return the dateDebut
     */
    public Date getDateDebut() {
	return dateDebut;
    }

    /**
     * @param dateDebut
     *            the dateDebut to set
     */
    public void setDateDebut(Date dateDebut) {
	this.dateDebut = dateDebut;
    }

    /**
     * @return the dateFin
     */
    public Date getDateFin() {
	return dateFin;
    }

    /**
     * @param dateFin
     *            the dateFin to set
     */
    public void setDateFin(Date dateFin) {
	this.dateFin = dateFin;
    }

    /**
     * @return the montant
     */
    public double getMontant() {
	return montant;
    }

    /**
     * @param montant
     *            the montant to set
     */
    public void setMontant(double montant) {
	this.montant = montant;
    }

    /**
     * @return the client
     */
    public Client getClient() {
	return client;
    }

    /**
     * @param client
     *            the client to set
     */
    public void setClient(Client client) {
	this.client = client;
    }

    /**
     * @return the refArticles
     */
    public List<String> getRefArticles() {
	return refArticles;
    }

    /**
     * @param refArticles
     *            the refArticles to set
     */
    public void setRefArticles(List<String> refArticles) {
	this.refArticles = refArticles;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "Location -> du " + dateDebut + " au " + dateFin + ", Cout " + montant + " € , "
		+ client.simpleToString() + ", liste -> " + refArticles;
    }

    public String toArchive() {
	return montant + "_" + dateDebut + "_" + dateFin + "_" + client + "_" + refArticles;
    }
}
