package Article;

import utils.Surface;

public class Reflecteur extends AccessoireLumiere {
    private Surface surface;

    /**
     * @param reference
     *            Référence du réflecteur
     * @param marque
     *            Marque du réflecteur
     * @param intitule
     *            Nom du réflecteur
     * @param prix
     *            Prix de la location
     * @param nbStock
     *            Quantité en stock
     * @param surface
     *            Surface du réflecteur
     */
    public Reflecteur(String reference, String marque, String intitule, double prix, int nbStock, Surface surface) {
	super(reference, marque, intitule, prix, nbStock);
	this.surface = surface;
    }

    /**
     * @return the surface
     */
    public Surface getSurface() {
	return surface;
    }

    /**
     * @param surface
     *            the surface to set
     */
    public void setSurface(Surface surface) {
	this.surface = surface;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return reference + " | Réflecteur -> " + intitule + " de " + marque + " , Surface: " + surface + " à " + prix
		+ " €/Jour, Qt " + nbStock;
    }
}
