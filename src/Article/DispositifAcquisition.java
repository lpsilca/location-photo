package Article;

import utils.TypeObjectif;

public class DispositifAcquisition extends Article {

    private double nbPixel;
    private TypeObjectif typeObjectif;

    /**
     * @param reference
     *            Référence du dispositif d'acquisition
     * @param marque
     *            Marque du dispositif d'acquisition
     * @param intitule
     *            Nom du dispositif d'acquisition
     * @param prix
     *            Prix de la location
     * @param nbStock
     *            Quantité en stock
     * @param nbPixel
     *            Nombre de pixel du dispositif d'acquisition
     */
    public DispositifAcquisition(String reference, String marque, String intitule, double prix, int nbStock,
	    double nbPixel, TypeObjectif typeObjectif) {
	super(reference, marque, intitule, prix, nbStock);
	this.nbPixel = nbPixel;
	this.typeObjectif = typeObjectif;
    }

    /**
     * @return the nbPixel
     */
    public double getNbPixel() {
	return nbPixel;
    }

    /**
     * @param nbPixel
     *            the nbPixel to set
     */
    public void setNbPixel(double nbPixel) {
	this.nbPixel = nbPixel;
    }

    /**
     * @return the typeObjectif
     */
    public TypeObjectif getTypeObjectif() {
	return typeObjectif;
    }

    /**
     * @param typeObjectif
     *            the typeObjectif to set
     */
    public void setTypeObjectif(TypeObjectif typeObjectif) {
	this.typeObjectif = typeObjectif;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return reference + " | Dispositif d'acquisition -> " + intitule + " de " + marque + " , " + nbPixel
		+ " Mpx, Zoom " + typeObjectif.toString() + " à " + prix + " €/Jour, Qt " + nbStock;
    }

}
