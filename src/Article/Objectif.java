package Article;

import utils.TypeObjectif;

public class Objectif extends Article {

    private TypeObjectif typeOjectif;

    /**
     * @param reference
     *            Référence de l'objectif
     * @param marque
     *            Marque de l'objectif
     * @param intitule
     *            Nom de l'objectif
     * @param prix
     *            Prix de la location
     * @param nbStock
     *            Quantité en stock
     * @param typeObjectif
     *            Type de l'objectif
     */
    public Objectif(String reference, String marque, String intitule, double prix, int nbStock,
	    TypeObjectif typeOjectif) {
	super(reference, marque, intitule, prix, nbStock);
	this.typeOjectif = typeOjectif;
    }

    /**
     * @return the typeOjectif
     */
    public TypeObjectif getTypeOjectif() {
	return typeOjectif;
    }

    /**
     * @param typeOjectif
     *            the typeOjectif to set
     */
    public void setTypeOjectif(TypeObjectif typeOjectif) {
	this.typeOjectif = typeOjectif;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return reference + " | Objectif -> " + intitule + " de " + marque + " , Zoom: " + typeOjectif + " à " + prix
		+ " €/Jour, Qt " + nbStock;
    }
}
