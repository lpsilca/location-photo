package Article;

public class PanneauLed extends AccessoireLumiere {
    private int nbLed;

    /**
     * @param reference
     *            Référence du panneau led
     * @param marque
     *            Marque du panneau led
     * @param intitule
     *            Nom du panneau led
     * @param prix
     *            Prix de la location
     * @param nbStock
     *            Quantité en stock
     * @param nbLed
     *            Nombre de led sur le panneau
     */
    public PanneauLed(String reference, String marque, String intitule, double prix, int nbStock, int nbLed) {
	super(reference, marque, intitule, prix, nbStock);
	this.nbLed = nbLed;
    }

    /**
     * @return the nbLed
     */
    public int getNbLed() {
	return nbLed;
    }

    /**
     * @param nbLed
     *            the nbLed to set
     */
    public void setNbLed(int nbLed) {
	this.nbLed = nbLed;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return reference + " | PanneauLed -> " + intitule + " de " + marque + " " + nbLed + " Led à " + prix
		+ " €/Jour, Qt " + nbStock;
    }
}
