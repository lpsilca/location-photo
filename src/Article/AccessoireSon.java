package Article;

public class AccessoireSon extends Article {

    /**
     * @param reference
     *            Référence de l'accessoire son
     * @param marque
     *            Marque de l'accessoire son
     * @param intitule
     *            Nom de l'accessoire son
     * @param prix
     *            Prix de la location
     * @param nbStock
     *            Quantité en stock
     */
    public AccessoireSon(String reference, String marque, String intitule, double prix, int nbStock) {
	super(reference, marque, intitule, prix, nbStock);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return reference + " | Accessoire Son -> " + intitule + " de " + marque + " à " + prix + " €/Jour, Qt "
		+ nbStock;
    }

}
