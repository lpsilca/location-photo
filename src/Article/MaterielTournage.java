package Article;

public class MaterielTournage extends Article {

    /**
     * @param reference
     *            Référence du matériel de tournage
     * @param marque
     *            Marque du matériel de tournage
     * @param intitule
     *            Nom du matériel de tournage
     * @param prix
     *            Prix de la location
     * @param nbStock
     *            Quantité en stock
     */
    public MaterielTournage(String reference, String marque, String intitule, double prix, int nbStock) {
	super(reference, marque, intitule, prix, nbStock);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return reference + " | Matériel de tournage -> " + intitule + " de " + marque + " à " + prix + " €/Jour, Qt "
		+ nbStock;
    }
}
