package Article;

public abstract class Article {

    protected String reference;
    protected String marque;
    protected String intitule;
    protected double prix;
    protected int nbStock;

    /**
     * @param reference
     *            Référence de l'article
     * @param marque
     *            Marque de l'article
     * @param intitule
     *            Nom de l'article
     * @param prix
     *            Prix de la location
     * @param nbStock
     *            Quantité en stock
     */
    public Article(String reference, String marque, String intitule, double prix, int nbStock) {
	super();
	this.reference = reference;
	this.marque = marque;
	this.intitule = intitule;
	this.prix = prix;
	this.nbStock = nbStock;
    }

    /**
     * @return the reference
     */
    public String getReference() {
	return reference;
    }

    /**
     * @param reference
     *            the reference to set
     */
    public void setReference(String reference) {
	this.reference = reference;
    }

    /**
     * @return the marque
     */
    public String getMarque() {
	return marque;
    }

    /**
     * @param marque
     *            the marque to set
     */
    public void setMarque(String marque) {
	this.marque = marque;
    }

    /**
     * @return the intitule
     */
    public String getIntitule() {
	return intitule;
    }

    /**
     * @param intitule
     *            the intitule to set
     */
    public void setIntitule(String intitule) {
	this.intitule = intitule;
    }

    /**
     * @return the prix
     */
    public double getPrix() {
	return prix;
    }

    /**
     * @param prix
     *            the prix to set
     */
    public void setPrix(double prix) {
	this.prix = prix;
    }

    /**
     * @return the nbStock
     */
    public int getNbStock() {
	return nbStock;
    }

    /**
     * @param nbStock
     *            the nbStock to set
     */
    public void setNbStock(int nbStock) {
	this.nbStock = nbStock;
    }

}
