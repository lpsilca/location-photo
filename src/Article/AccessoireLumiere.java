package Article;

public abstract class AccessoireLumiere extends Article {

    /**
     * @param reference
     *            Référence de l'accessoire lumière
     * @param marque
     *            Marque de l'accessoire lumière
     * @param intitule
     *            Nom de l'accessoire lumière
     * @param prix
     *            Prix de la location
     * @param nbStock
     *            Quantité en stock
     */
    public AccessoireLumiere(String reference, String marque, String intitule, double prix, int nbStock) {
	super(reference, marque, intitule, prix, nbStock);
    }

}
