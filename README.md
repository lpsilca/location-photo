# LP SIL Project
>	This is a java project of an application to leasing some tools for doing photo or filming

---

## Fonctionnalitées principale de l'application:
*	Ajouter/Supprimer un client
*	Ajouter/Supprimer un article
*	Enregistrer une location (qui débute à la date du jour)
*	Terminer une location (qui est archiver dans un fichier .loc)
*	Afficher une archive

Elles sont disponible sous forme d'un menu avec un numéro les désignants divisé sous deux sous menu
un pour la gestion des clients et l'autre pour le gestion des locations

*NB : Si vous souhaitez ajouter, modifer ou supprimer des articles, allez dans le fichier* **conf/Articles.conf** 

### Pour essayer l'application (fonctionnalité archivage) :
#### Archiver une location
*	Menu Location 						(2)
*	Terminer une location				(4)
*	Le première location de la liste 	(0)

####	Afficher l'archive (à partir du menu principal)
*	Menu Location		(2)
*	Affiche une archive	(3)
*	saisir " 09/2016 "

####	Afficher son CA (à partir du menu principal)
*	Consulter le chiffre d'affaire d'un période	(4)
*	saisir " 09/2016 "
*	saisir " 10/2016 "